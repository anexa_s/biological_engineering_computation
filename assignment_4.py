import re
import math


def load_tree(filename):
    f = open(filename, 'r')
    exp = f.read()
    f.close()

    exp = exp.replace(';', '')
    exp = re.sub(r'\s+', '', exp)
    exp = re.sub(r'\n', '', exp)
    exp = re.sub(r'\[.*\]', '', exp)

    return parse_tree(exp)


def make_leaf(name, length):
    return {'left': None, 'right': None, 'name': name, 'length': length}


def parse_tree(exp):
    if ',' not in exp:
        name, length = exp.split(':')
        length = float(length)
        return make_leaf(name, length)

    dist_pattern = re.compile(r'(?P<tree>\(.+\))\:(?P<length>[e\-\d\.]+)$')
    m = dist_pattern.search(exp)
    length = 0
    if m:
        if m.group('length'):
            length = float(m.group('length'))
        exp = m.group('tree')
    if length == '':
        length = 0
    lhs, rhs = parse_exp(exp)
    return {"name": "internal",
            "left": parse_tree(lhs),
            "right": parse_tree(rhs),
            "length": length}


def parse_exp(exp):
    chars = list(exp[1:-1])
    count = 0
    lhs = True
    left = ''
    right = ''

    for c in chars:
        if c == '(':
            count += 1
        if c == ')':
            count -= 1
        if (c == ',') and (count == 0) and (lhs):
            lhs = False
            continue

        if lhs:
            left += c
        else:
            right += c

    return [left, right]


def read_alignment(filename):
    f = open(filename, "r")
    taxa = None
    columns = None
    sequences = {}

    for line in f:
        if taxa is None:
            taxa, columns = line.split()
        else:
            words = line.split()
            name = words[0]
            seq = ''.join(words[1:])
            if name in sequences.keys():
                sequences[name] += seq
            else:
                sequences[name] = seq

    return sequences


def init_tree(tree, aln):
    if tree['name'] != 'internal':
        chars = aln[tree['name']]
        tree['data'] = [[chars[x]] for x in range(0, len(chars))]
        return
    init_tree(tree['left'], aln)
    init_tree(tree['right'], aln)


def down_pass(tree):
    if tree['name'] != 'internal':
        return 0
    left_cost = down_pass(tree['left'])
    right_cost = down_pass(tree['right'])

    left_seq = tree['left']['data']
    right_seq = tree['right']['data']
    n = len(left_seq)

    mutations = 0
    seq = []
    for i in range(n):
        seq_i = []
        left_i = set(left_seq[i])
        right_i = set(right_seq[i])
        intersection = set()

        for x in left_i:
            if x in right_i:
                intersection.add(x)

        if len(intersection) == 0:
            mutations += 1
            for x in left_i:
                seq_i.append(x)
            for x in right_i:
                seq_i.append(x)
        else:
            for x in intersection:
                seq_i.append(x)
        seq.append(seq_i)

    tree['data'] = seq
    return mutations + left_cost + right_cost


tree = load_tree('tree4.txt')
aln = read_alignment('seqs.aln')

init_tree(tree, aln)
print('Number of mutations: %d' % (down_pass(tree)))
print('Sequence at root: %s' % ','.join(['/'.join(x) for x in tree['data']]))
