
# Q_1
def fib(k, first, second):
    if k == 1:
        return [first]
    if k == 2:
        return [second] + fib(k - 1, first, second)
    l = fib(k - 1, first, second)
    return [l[0] + l[1]] + l


# Q_2
class Subtree:
    def __init__(self, seq=""):
        self.is_leaf = (len(seq) != 0)
        self.seq = seq
        self.children = []

    def insert(self, subtree):
        self.children.append(subtree)

    def get_nodes(self):
        if self.is_leaf:
            return [self]
        nodes = []
        for child in self.children:
            nodes = nodes + child.get_nodes()
        return nodes

    @staticmethod
    def distance_nodes(a, b):
        assert (len(a.seq) == len(b.seq))
        cnt = 0
        for i in range(len(a.seq)):
            cnt += (a.seq[i] != b.seq[i])
        return cnt

    @staticmethod
    def distance(a, b):
        a_nodes = a.get_nodes()
        b_nodes = b.get_nodes()

        dist = 0
        for a_node in a_nodes:
            for b_node in b_nodes:
                dist += Subtree.distance_nodes(a_node, b_node)

        dist /= (len(a_nodes) * len(b_nodes))
        return dist


nodes = [Subtree("AAGGCCCACTA"), Subtree("GATGTCCGATA"), Subtree("AAGGCCCACTT"), Subtree("AATGGCCCCTA"),
         Subtree("GATGTCCGATA")]

while len(nodes) != 1:
    mn = 100
    pos_i = -1
    pos_j = -1
    for i in range(len(nodes)):
        for j in range(i + 1, len(nodes)):
            if mn > Subtree.distance(nodes[i], nodes[j]):
                mn = Subtree.distance(nodes[i], nodes[j])
                pos_i = i
                pos_j = j
    new_root = Subtree()
    new_root.insert(nodes[pos_i])
    new_root.insert(nodes[pos_j])
    print("merging", [node.seq for node in nodes[pos_i].get_nodes()], "with", [node.seq for node in nodes[pos_j].get_nodes()])
    new_nodes = []
    for i in range(len(nodes)):
        if i != pos_i and i != pos_j:
            new_nodes.append(nodes[i])
    new_nodes.append(new_root)
    nodes = new_nodes

"""
         o4
      /      \
     /        o3
    /        / \
   o1       o2  4
  / \      / \
 2   5    1   3   

"""