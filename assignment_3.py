import re
import math


def load_tree(filename):
    f = open(filename, 'r')
    exp = f.read()
    f.close()

    exp = exp.replace(';', '')
    exp = re.sub(r'\s+', '', exp)
    exp = re.sub(r'\n', '', exp)
    exp = re.sub(r'\[.*\]', '', exp)

    return parse_tree(exp)


def make_leaf(name, length):
    return {'left': None, 'right': None, 'name': name, 'length': length}


def parse_tree(exp):
    if ',' not in exp:
        name, length = exp.split(':')
        length = float(length)
        return make_leaf(name, length)

    dist_pattern = re.compile(r'(?P<tree>\(.+\))\:(?P<length>[e\-\d\.]+)$')
    m = dist_pattern.search(exp)
    length = 0
    if m:
        if m.group('length'):
            length = float(m.group('length'))
        exp = m.group('tree')
    if length == '':
        length = 0
    lhs, rhs = parse_exp(exp)
    return {"name": "internal",
            "left": parse_tree(lhs),
            "right": parse_tree(rhs),
            "length": length}


def parse_exp(exp):
    chars = list(exp[1:-1])
    count = 0
    lhs = True
    left = ''
    right = ''

    for c in chars:
        if c == '(':
            count += 1
        if c == ')':
            count -= 1
        if (c == ',') and (count == 0) and (lhs):
            lhs = False
            continue

        if lhs:
            left += c
        else:
            right += c

    return [left, right]


def distance(tree):
    if tree['name'] != 'internal':
        return float(tree['length']), 1
    else:
        sum_l, cnt_l = distance(tree['left'])
        sum_r, cnt_r = distance(tree['right'])
        return sum_l + sum_r + (cnt_l + cnt_r) * float(tree['length']), cnt_l + cnt_r


def avg_distance(tree):
    sum, cnt = distance(tree)
    return sum / cnt


def min_distance(tree):
    if tree['name'] != 'internal':
        return [float(tree['length']), tree['name']]
    else:
        [mn_l, name_l] = min_distance(tree['left'])
        [mn_r, name_r] = min_distance(tree['right'])
        if mn_l <= mn_r:
            return [mn_l + float(tree['length']), name_l]
        else:
            return [mn_r + float(tree['length']), name_r]


def max_distance(tree):
    if tree['name'] != 'internal':
        return [float(tree['length']), tree['name']]
    else:
        [mn_l, name_l] = max_distance(tree['left'])
        [mn_r, name_r] = max_distance(tree['right'])
        if mn_l >= mn_r:
            return [mn_l + float(tree['length']), name_l]
        else:
            return [mn_r + float(tree['length']), name_r]


tree = load_tree('tree3.txt')

print("Average distance:", avg_distance(tree))
print("Min distance/node", min_distance(tree))
print("Max distance/node", max_distance(tree))
