import re
import math


def load_tree(filename):
    f = open(filename, 'r')
    exp = f.read()
    f.close()

    exp = exp.replace(';', '')
    exp = re.sub(r'\s+', '', exp)
    exp = re.sub(r'\n', '', exp)
    exp = re.sub(r'\[.*\]', '', exp)

    return parse_tree(exp)


def make_leaf(name, length):
    return {'left': None, 'right': None, 'name': name, 'length': length}


def parse_tree(exp):
    if ',' not in exp:
        name, length = exp.split(':')
        length = float(length)
        return make_leaf(name, length)

    dist_pattern = re.compile(r'(?P<tree>\(.+\))\:(?P<length>[e\-\d\.]+)$')
    m = dist_pattern.search(exp)
    length = 0
    if m:
        if m.group('length'):
            length = float(m.group('length'))
        exp = m.group('tree')
    if length == '':
        length = 0
    lhs, rhs = parse_exp(exp)
    return {"name": "internal",
            "left": parse_tree(lhs),
            "right": parse_tree(rhs),
            "length": length}


def parse_exp(exp):
    chars = list(exp[1:-1])
    count = 0
    lhs = True
    left = ''
    right = ''

    for c in chars:
        if c == '(':
            count += 1
        if c == ')':
            count -= 1
        if (c == ',') and (count == 0) and (lhs):
            lhs = False
            continue

        if lhs:
            left += c
        else:
            right += c

    return [left, right]


def read_alignment(filename):
    f = open(filename, "r")
    taxa = None
    columns = None
    sequences = {}

    for line in f:
        if taxa is None:
            taxa, columns = line.split()
        else:
            words = line.split()
            name = words[0]
            seq = ''.join(words[1:])
            if name in sequences.keys():
                sequences[name] += seq
            else:
                sequences[name] = seq

    return sequences


def init_tree(tree, aln):
    if tree['name'] != 'internal':
        chars = aln[tree['name']]
        tree['data'] = [[chars[x]] for x in range(0, len(chars))]
        return len(chars)
    init_tree(tree['left'], aln)
    length = init_tree(tree['right'], aln)
    return length


def evo_model(x, y, distance):
    if x == y:
        return (1 + 3 * math.exp(-4 * distance)) / 4
    else:
        return 3 * (1 - math.exp(-4 * distance)) / 4


def ml(tree, pos):
    if tree['name'] != 'internal':
        likelihood = {}
        for n in ['A', 'C', 'G', 'T']:
            if [n] == tree['data'][pos]:
                likelihood[n] = 1
            else:
                likelihood[n] = 0
        return likelihood
    else:
        likelihood = {}
        likelihood_l = ml(tree['left'], pos)
        likelihood_r = ml(tree['right'], pos)
        bases = ['A', 'C', 'G', 'T']
        for n in bases:
            likelihood[n] = 0
            for l in bases:
                for r in bases:
                    likelihood[n] += evo_model(n, l, tree['left']['length']) * likelihood_l[l] * evo_model(n, r, tree['right']['length']) * likelihood_r[r]
        return likelihood


def max_item(x):
    mx = 0
    max_key = None
    for k, v in x.items():
        if v > mx:
            mx, max_key = v, k
    return mx, max_key


tree = load_tree('tree5.txt')
aln = read_alignment('seqs5.aln')

alnLength = init_tree(tree, aln)

print("(Likelihood of site, Best sequence at root)")
for pos in range(alnLength):
    print(max_item(ml(tree, pos)))
