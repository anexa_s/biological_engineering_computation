import re
import math


def load_tree(filename):
    f = open(filename, 'r')
    exp = f.read()
    f.close()

    exp = exp.replace(';', '')
    exp = re.sub(r'\s+', '', exp)
    exp = re.sub(r'\n', '', exp)
    exp = re.sub(r'\[.*\]', '', exp)

    return parse_tree(exp)


def make_leaf(name, length):
    return {'left': None, 'right': None, 'name': name, 'length': length}


def parse_tree(exp):
    if ',' not in exp:
        name, length = exp.split(':')
        length = float(length)
        return make_leaf(name, length)

    dist_pattern = re.compile(r'(?P<tree>\(.+\))\:(?P<length>[e\-\d\.]+)$')
    m = dist_pattern.search(exp)
    length = 0
    if m:
        if m.group('length'):
            length = float(m.group('length'))
        exp = m.group('tree')
    if length == '':
        length = 0
    lhs, rhs = parse_exp(exp)
    return {"name": "internal",
            "left": parse_tree(lhs),
            "right": parse_tree(rhs),
            "length": length}


def parse_exp(exp):
    chars = list(exp[1:-1])
    count = 0
    lhs = True
    left = ''
    right = ''

    for c in chars:
        if c == '(':
            count += 1
        if c == ')':
            count -= 1
        if (c == ',') and (count == 0) and (lhs):
            lhs = False
            continue

        if lhs:
            left += c
        else:
            right += c

    return [left, right]


def print_tree(tree):
    tree_str = tree_2_string(tree)
    print(tree_str + ";")


def tree_2_string(tree):
    if tree['name'] != 'internal':
        return tree['name'] + ':' + str(tree['length'])
    else:
        left = tree_2_string(tree['left'])
        right = tree_2_string(tree['right'])
        return '(' + left + ',' + right + ')' + ':' + str(tree['length'])


tree = load_tree('tree.txt')
print_tree(tree)
